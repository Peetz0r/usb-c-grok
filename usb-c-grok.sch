EESchema Schematic File Version 2
LIBS:zetex
LIBS:xilinx
LIBS:wiznet
LIBS:video
LIBS:valves
LIBS:ttl_ieee
LIBS:triac_thyristor
LIBS:transistors
LIBS:transf
LIBS:texas
LIBS:switches
LIBS:supertex
LIBS:stm8
LIBS:stm32
LIBS:siliconi
LIBS:silabs
LIBS:sensors
LIBS:rfcom
LIBS:relays
LIBS:regul
LIBS:references
LIBS:powerint
LIBS:power
LIBS:philips
LIBS:opto
LIBS:onsemi
LIBS:nxp_armmcu
LIBS:nxp
LIBS:nordicsemi
LIBS:msp430
LIBS:motors
LIBS:motorola
LIBS:motor_drivers
LIBS:modules
LIBS:microcontrollers
LIBS:microchip_pic32mcu
LIBS:microchip_pic24mcu
LIBS:pspice
LIBS:microchip_pic18mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic10mcu
LIBS:microchip_dspic33dsc
LIBS:microchip
LIBS:memory
LIBS:mechanical
LIBS:maxim
LIBS:logic_programmable
LIBS:linear
LIBS:leds
LIBS:ir
LIBS:intersil
LIBS:interface
LIBS:intel
LIBS:infineon
LIBS:hc11
LIBS:graphic_symbols
LIBS:gennum
LIBS:ftdi
LIBS:elec-unifil
LIBS:dsp
LIBS:display
LIBS:diode
LIBS:digital-audio
LIBS:device
LIBS:dc-dc
LIBS:cypress
LIBS:contrib
LIBS:conn
LIBS:cmos_ieee
LIBS:cmos4000
LIBS:brooktre
LIBS:bosch
LIBS:bbd
LIBS:battery_management
LIBS:audio
LIBS:atmel
LIBS:analog_switches
LIBS:analog_devices
LIBS:allegro
LIBS:adc-dac
LIBS:actel
LIBS:ac-dc
LIBS:Zilog
LIBS:Xicor
LIBS:Worldsemi
LIBS:RFSolutions
LIBS:Power_Management
LIBS:Oscillators
LIBS:Lattice
LIBS:LEM
LIBS:ESD_Protection
LIBS:Altera
LIBS:74xx
LIBS:74xgxx
LIBS:usb-c-grok-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "usb-c-grok"
Date "2018-07-20"
Rev "10"
Comp "Peter Hazenberg"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR01
U 1 1 5B5268A3
P 9850 1400
F 0 "#PWR01" H 9850 1150 50  0001 C CNN
F 1 "GND" H 9950 1250 50  0000 R CNN
F 2 "" H 9850 1400 50  0001 C CNN
F 3 "" H 9850 1400 50  0001 C CNN
	1    9850 1400
	-1   0    0    1   
$EndComp
$Comp
L +3V3 #PWR02
U 1 1 5B527D25
P 7700 1450
F 0 "#PWR02" H 7700 1300 50  0001 C CNN
F 1 "+3V3" H 7850 1500 50  0000 C CNN
F 2 "" H 7700 1450 50  0001 C CNN
F 3 "" H 7700 1450 50  0001 C CNN
	1    7700 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5B5284D6
P 7600 3050
F 0 "#PWR03" H 7600 2800 50  0001 C CNN
F 1 "GND" H 7650 2900 50  0000 R CNN
F 2 "" H 7600 3050 50  0001 C CNN
F 3 "" H 7600 3050 50  0001 C CNN
	1    7600 3050
	1    0    0    -1  
$EndComp
$Comp
L power:+1V8 #PWR04
U 1 1 5B52945F
P 7500 1450
F 0 "#PWR04" H 7500 1300 50  0001 C CNN
F 1 "+1V8" H 7350 1500 50  0000 C CNN
F 2 "" H 7500 1450 50  0001 C CNN
F 3 "" H 7500 1450 50  0001 C CNN
	1    7500 1450
	1    0    0    -1  
$EndComp
$Comp
L Logic_LevelTranslator:TXB0104D U1
U 1 1 5B529DC8
P 7600 2250
F 0 "U1" H 8000 1700 50  0000 C CNN
F 1 "TXB0104D" H 7850 1600 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 7650 1400 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/txb0104.pdf" H 7710 2345 50  0001 C CNN
	1    7600 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5B52E568
P 2000 3500
F 0 "#PWR05" H 2000 3250 50  0001 C CNN
F 1 "GND" H 2005 3327 50  0000 C CNN
F 2 "" H 2000 3500 50  0001 C CNN
F 3 "" H 2000 3500 50  0001 C CNN
	1    2000 3500
	1    0    0    -1  
$EndComp
$Comp
L LM1117-1.8 U2
U 1 1 5B51501C
P 7750 5050
F 0 "U2" H 7600 5175 50  0000 C CNN
F 1 "LM1117-1.8" H 7550 5250 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-223-3Lead_TabPin2" H 7750 5050 50  0001 C CNN
F 3 "" H 7750 5050 50  0001 C CNN
	1    7750 5050
	1    0    0    -1  
$EndComp
$Comp
L C_Small C2
U 1 1 5B51531B
P 8100 5200
F 0 "C2" H 8110 5270 50  0000 L CNN
F 1 "22µF" H 8110 5120 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 8100 5200 50  0001 C CNN
F 3 "" H 8100 5200 50  0001 C CNN
	1    8100 5200
	1    0    0    -1  
$EndComp
$Comp
L C_Small C1
U 1 1 5B5153B9
P 7400 5200
F 0 "C1" H 7410 5270 50  0000 L CNN
F 1 "22µF" H 7410 5120 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7400 5200 50  0001 C CNN
F 3 "" H 7400 5200 50  0001 C CNN
	1    7400 5200
	1    0    0    -1  
$EndComp
$Comp
L +1V8 #PWR06
U 1 1 5B5155AC
P 8150 5050
F 0 "#PWR06" H 8150 4900 50  0001 C CNN
F 1 "+1V8" H 8150 5190 50  0000 C CNN
F 2 "" H 8150 5050 50  0001 C CNN
F 3 "" H 8150 5050 50  0001 C CNN
	1    8150 5050
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR07
U 1 1 5B515620
P 7350 5050
F 0 "#PWR07" H 7350 4900 50  0001 C CNN
F 1 "+3V3" H 7350 5190 50  0000 C CNN
F 2 "" H 7350 5050 50  0001 C CNN
F 3 "" H 7350 5050 50  0001 C CNN
	1    7350 5050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 5B5156A5
P 7750 5450
F 0 "#PWR08" H 7750 5200 50  0001 C CNN
F 1 "GND" H 7750 5300 50  0000 C CNN
F 2 "" H 7750 5450 50  0001 C CNN
F 3 "" H 7750 5450 50  0001 C CNN
	1    7750 5450
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR09
U 1 1 5B52BB23
P 9250 2200
F 0 "#PWR09" H 9250 2050 50  0001 C CNN
F 1 "+3V3" H 9250 2340 50  0000 C CNN
F 2 "" H 9250 2200 50  0001 C CNN
F 3 "" H 9250 2200 50  0001 C CNN
	1    9250 2200
	1    0    0    -1  
$EndComp
$Comp
L USB_C_Receptacle P2
U 1 1 5B5F58B8
P 3600 2950
F 0 "P2" V 3350 4200 50  0000 L CNN
F 1 "USB_C_Receptacle" V 3250 4300 50  0000 R CNN
F 2 "usb-c-grok:USB_C_Receptacle_PCBEdge_Alt" H 3750 2950 50  0001 C CNN
F 3 "" H 3750 2950 50  0001 C CNN
	1    3600 2950
	0    1    -1   0   
$EndComp
$Comp
L USB_C_Plug P1
U 1 1 5B5F9437
P 3600 1150
F 0 "P1" V 3350 2400 50  0000 L CNN
F 1 "USB_C_Plug" V 3250 2500 50  0000 R CNN
F 2 "usb-c-grok:USB_C_Plug_Molex_105444_HandSoldering" H 3750 1150 50  0001 C CNN
F 3 "" H 3750 1150 50  0001 C CNN
	1    3600 1150
	0    1    1    0   
$EndComp
$Comp
L INA219 U4
U 1 1 5B5FD216
P 5950 2750
F 0 "U4" H 5950 2300 60  0000 C CNN
F 1 "INA219" H 5950 3200 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-8_Handsoldering" H 5950 2750 60  0001 C CNN
F 3 "" H 5950 2750 60  0001 C CNN
	1    5950 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5B5FB0A4
P 5400 2850
F 0 "#PWR010" H 5400 2600 50  0001 C CNN
F 1 "GND" V 5405 2722 50  0000 R CNN
F 2 "" H 5400 2850 50  0001 C CNN
F 3 "" H 5400 2850 50  0001 C CNN
	1    5400 2850
	0    1    1    0   
$EndComp
$Comp
L +3V3 #PWR011
U 1 1 5B5FB0FD
P 5400 3050
F 0 "#PWR011" H 5400 2900 50  0001 C CNN
F 1 "+3V3" V 5400 3300 50  0000 C CNN
F 2 "" H 5400 3050 50  0001 C CNN
F 3 "" H 5400 3050 50  0001 C CNN
	1    5400 3050
	0    -1   -1   0   
$EndComp
$Comp
L R_Small R1
U 1 1 5B5FE58F
P 5300 2350
F 0 "R1" V 5350 2300 50  0000 L CNN
F 1 "0.01" V 5300 2350 39  0000 C CNN
F 2 "Resistors_SMD:R_2512_HandSoldering" H 5300 2350 50  0001 C CNN
F 3 "" H 5300 2350 50  0001 C CNN
	1    5300 2350
	0    1    1    0   
$EndComp
$Comp
L C_Small C3
U 1 1 5B5FDAB7
P 5500 2950
F 0 "C3" H 5510 3020 50  0000 L CNN
F 1 "22µf" H 5150 2950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5500 2950 50  0001 C CNN
F 3 "" H 5500 2950 50  0001 C CNN
	1    5500 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5B5FE7AE
P 6350 3150
F 0 "#PWR012" H 6350 2900 50  0001 C CNN
F 1 "GND" H 6400 3000 50  0000 R CNN
F 2 "" H 6350 3150 50  0001 C CNN
F 3 "" H 6350 3150 50  0001 C CNN
	1    6350 3150
	1    0    0    -1  
$EndComp
Text Label 2300 2050 1    60   ~ 0
SBU2
Text Label 2400 2050 1    60   ~ 0
SBU1
Text Label 2600 2050 1    60   ~ 0
TX2+
Text Label 2700 2050 1    60   ~ 0
TX2-
Text Label 2900 2050 1    60   ~ 0
RX2+
Text Label 3000 2050 1    60   ~ 0
RX2-
Text Label 3200 2050 1    60   ~ 0
TX1+
Text Label 3300 2050 1    60   ~ 0
TX1-
Text Label 3500 2050 1    60   ~ 0
RX1+
Text Label 3600 2050 1    60   ~ 0
RX1-
Text Label 3900 2050 1    60   ~ 0
D+
Text Label 4100 2050 1    60   ~ 0
D-
Text Label 4750 2150 2    60   ~ 0
CC2_L
Text Label 4750 1950 2    60   ~ 0
CC1_L
Text Label 5000 1800 0    60   ~ 0
VBUS+
Text Label 5000 2300 0    60   ~ 0
VBUS-
Text Label 8150 1950 0    60   ~ 0
CC1_H
Text Label 8150 2150 0    60   ~ 0
CC2_H
Text Label 6500 2650 2    60   ~ 0
SDA
Text Label 6500 2450 2    60   ~ 0
SCL
Connection ~ 5200 2350
Connection ~ 5400 2350
Wire Wire Line
	5200 2450 5600 2450
Wire Wire Line
	5400 2650 5600 2650
Connection ~ 9650 2650
Connection ~ 9650 1450
Wire Wire Line
	9150 2550 9550 2550
Wire Wire Line
	8850 2650 9750 2650
Wire Wire Line
	8850 2650 8850 3500
Wire Wire Line
	8700 1450 9750 1450
Wire Wire Line
	8700 3350 8700 1450
Wire Wire Line
	4600 1800 5400 1800
Wire Wire Line
	5200 2300 5200 2450
Wire Wire Line
	4600 2300 5200 2300
Wire Wire Line
	5400 1800 5400 2650
Wire Wire Line
	6350 2850 6350 3150
Connection ~ 5500 3050
Wire Wire Line
	5400 3050 5600 3050
Connection ~ 5500 2850
Wire Wire Line
	5400 2850 5600 2850
Wire Wire Line
	6650 3350 8700 3350
Wire Wire Line
	8850 3500 6500 3500
Wire Wire Line
	6300 2450 6650 2450
Wire Wire Line
	6300 2650 6500 2650
Connection ~ 6350 3050
Wire Wire Line
	6350 3050 6300 3050
Wire Wire Line
	6300 2850 6350 2850
Connection ~ 2000 1250
Connection ~ 2000 1150
Connection ~ 2000 1050
Connection ~ 2000 950 
Wire Wire Line
	4900 2300 4900 2350
Wire Wire Line
	4800 2300 4800 2350
Wire Wire Line
	4700 2300 4700 2350
Wire Wire Line
	4600 2350 4600 2300
Wire Wire Line
	4900 1800 4900 1750
Wire Wire Line
	4800 1800 4800 1750
Wire Wire Line
	4700 1800 4700 1750
Wire Wire Line
	4600 1800 4600 1750
Connection ~ 4900 2300
Connection ~ 4800 2300
Connection ~ 4600 2300
Connection ~ 4700 2300
Wire Wire Line
	8000 2150 9150 2150
Wire Wire Line
	8000 1950 9150 1950
Connection ~ 2000 3250
Connection ~ 4100 2250
Wire Wire Line
	4000 2250 4000 2350
Wire Wire Line
	4100 2250 4000 2250
Connection ~ 3900 2250
Wire Wire Line
	3800 2250 3900 2250
Wire Wire Line
	3800 2350 3800 2250
Wire Wire Line
	4400 1950 7200 1950
Wire Wire Line
	4300 2150 7200 2150
Wire Wire Line
	9150 2150 9150 2550
Wire Wire Line
	9150 1950 9150 1550
Connection ~ 4300 2150
Connection ~ 4400 1950
Connection ~ 4600 1800
Connection ~ 4700 1800
Connection ~ 4900 1800
Connection ~ 4800 1800
Wire Wire Line
	4400 1750 4400 2350
Wire Wire Line
	4300 1750 4300 2350
Wire Wire Line
	4100 1750 4100 2350
Wire Wire Line
	3900 1750 3900 2350
Wire Wire Line
	3600 1750 3600 2350
Wire Wire Line
	3500 1750 3500 2350
Wire Wire Line
	3300 1750 3300 2350
Wire Wire Line
	3200 1750 3200 2350
Wire Wire Line
	3000 1750 3000 2350
Wire Wire Line
	2900 1750 2900 2350
Wire Wire Line
	2700 1750 2700 2350
Wire Wire Line
	2600 1750 2600 2350
Wire Wire Line
	2300 1750 2300 2350
Wire Wire Line
	2400 1750 2400 2350
Connection ~ 2000 3150
Connection ~ 2000 3050
Connection ~ 2000 2950
Connection ~ 2000 2850
Wire Wire Line
	2000 850  2000 3500
Wire Wire Line
	9250 2450 9450 2450
Wire Wire Line
	9150 1550 9550 1550
Wire Wire Line
	9650 1450 9650 1650
Wire Wire Line
	9550 1550 9550 1650
Wire Wire Line
	9750 1450 9750 1650
Connection ~ 7400 5050
Connection ~ 8100 5050
Connection ~ 7750 5400
Wire Wire Line
	7400 5400 8100 5400
Wire Wire Line
	7750 5350 7750 5450
Wire Wire Line
	7400 5300 7400 5400
Wire Wire Line
	8100 5400 8100 5300
Wire Wire Line
	8100 5050 8100 5100
Wire Wire Line
	8050 5050 8150 5050
Wire Wire Line
	7350 5050 7450 5050
Wire Wire Line
	7400 5100 7400 5050
Wire Wire Line
	7700 1450 7700 1550
Wire Wire Line
	7500 1450 7500 1550
Wire Wire Line
	7600 2950 7600 3050
Wire Wire Line
	9850 1400 9850 1650
Wire Wire Line
	7200 1750 7100 1750
Wire Wire Line
	7100 1750 7100 1500
Wire Wire Line
	7100 1500 7500 1500
Connection ~ 7500 1500
Wire Wire Line
	9450 2450 9450 2400
Wire Wire Line
	9550 2550 9550 2400
Wire Wire Line
	9750 2650 9750 2400
Wire Wire Line
	9650 2650 9650 2400
$Comp
L Conn_01x05 J2
U 1 1 5B8C70F8
P 9650 2200
F 0 "J2" H 9650 2500 50  0000 C CNN
F 1 "Conn_01x05" H 9650 1900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 9650 2200 50  0001 C CNN
F 3 "" H 9650 2200 50  0001 C CNN
	1    9650 2200
	0    -1   -1   0   
$EndComp
$Comp
L Conn_01x05 J1
U 1 1 5B8C716F
P 9650 1850
F 0 "J1" H 9650 2150 50  0000 C CNN
F 1 "Conn_01x05" H 9650 1550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 9650 1850 50  0001 C CNN
F 3 "" H 9650 1850 50  0001 C CNN
	1    9650 1850
	0    -1   1    0   
$EndComp
Wire Wire Line
	9250 2200 9250 2450
Wire Wire Line
	6500 2650 6500 4250
Wire Wire Line
	6650 2450 6650 4150
Wire Wire Line
	7200 2350 7100 2350
Wire Wire Line
	7100 2350 7100 3000
Wire Wire Line
	7100 2550 7200 2550
Wire Wire Line
	7100 3000 8150 3000
Connection ~ 7600 3000
Connection ~ 7100 2550
Wire Wire Line
	8000 2350 8150 2350
Wire Wire Line
	8150 2350 8150 3000
Wire Wire Line
	8150 2550 8000 2550
Connection ~ 8150 2550
$Comp
L Conn_01x06_Female J3
U 1 1 5B909E5F
P 7600 3950
F 0 "J3" H 7600 4250 50  0000 C CNN
F 1 "Conn_01x06_Female" H 7600 3550 50  0000 C CNN
F 2 "usb-c-grok:Socket_Strip_Angled_1x06_Pitch2.54mm_SMD" H 7600 3950 50  0001 C CNN
F 3 "" H 7600 3950 50  0001 C CNN
	1    7600 3950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 5B90A03A
P 7350 3750
F 0 "#PWR013" H 7350 3500 50  0001 C CNN
F 1 "GND" V 7350 3550 50  0000 C CNN
F 2 "" H 7350 3750 50  0001 C CNN
F 3 "" H 7350 3750 50  0001 C CNN
	1    7350 3750
	0    1    1    0   
$EndComp
$Comp
L +3V3 #PWR014
U 1 1 5B90A077
P 7350 3850
F 0 "#PWR014" H 7350 3700 50  0001 C CNN
F 1 "+3V3" V 7350 4050 50  0000 C CNN
F 2 "" H 7350 3850 50  0001 C CNN
F 3 "" H 7350 3850 50  0001 C CNN
	1    7350 3850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6800 2150 6800 4050
Connection ~ 6800 2150
Wire Wire Line
	6950 1950 6950 3950
Wire Wire Line
	6950 3950 7400 3950
Connection ~ 6950 1950
Wire Wire Line
	7350 3850 7400 3850
Wire Wire Line
	7350 3750 7400 3750
Wire Wire Line
	6800 4050 7400 4050
Wire Wire Line
	6650 4150 7400 4150
Connection ~ 6650 3350
Wire Wire Line
	6500 4250 7400 4250
Connection ~ 6500 3500
$EndSCHEMATC
